# Publish Documentation on ReadTheDocs

## 1 Create a ReadTheDocs Config File

`.readthedocs.yaml`:

```yaml
version: 2

build:
  os: "ubuntu-22.04"
  tools:
    python: "3.11"

sphinx:
  configuration: source/conf.py

python:
  install:
    - requirements: source/requirements.txt
```

## 2 Create a ReadTheDocs Account

## 3 Create a ReadTheDocs Project

## 4 Create a ReadTheDocs API-Token

## 5 Add API-Token to CI Variables in GitLab Project Settings

## 6 Add CI Pipeline

```yaml
stages:
  - deploy

readthedocs:
  image: alpine:latest
  stage: deploy
  before_script:
    - apk add curl
  script:
    - 'curl -X POST -H "Authorization: Token $RTD_TOKEN" https://readthedocs.org/api/v3/projects/my-readthedocs-demo/versions/latest/builds/'
  rules:
    # Only run the job on the default branch and not in forks.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH == "hueser93/my-readthedocs-demo"
```
